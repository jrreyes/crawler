import requests
import json
from bs4 import BeautifulSoup
import sys
import pyodbc
import database as db
import auxiliar as af
import traceback
import logging
import time
import datetime

reload(sys)
sys.setdefaultencoding('utf8')


'''
Clase que permite parsear informacion de una orden de compra
'''
class ParserOC:
    max_tries = 4
    def __init__(self,url,api,code,name,conn,in_db=False):
        self.url = url
        self.api = api
        self.code = code
        self.valid_api = False
        self.output_name = name
        self.conn = conn
        self.error = False
        self.in_db = in_db
        self.ticket_overflow=False
        print api


    def request_API(self):
        '''
        Obtiene el json asociado al request
        '''
        i=0
        while True:
            i+=1
            try:
                self.json_info=requests.get(self.api).json()
                break
            except:
                logging.error("error en la conexion con la api se seguira intentando, codigo %s\n%s" % (self.code, traceback.format_exc()))
                if i == self.max_tries/2:
                    time.sleep(2)
                
                elif i >= self.max_tries:
                    logging.error("error inesperado con la api, codigo %s\n%s" % (self.code, traceback.format_exc()))
                    self.valid_api = False
                    return self.valid_api
        try:

            mens = self.json_info.get('Mensaje')
            if not mens is None:
                self.ticket_overflow = True

            aux = self.json_info['Cantidad']
            if aux == 0:
                self.valid_api = False
                logging.error('api entrega respuesta mal formada, codigo %s\n%s' % (self.code, traceback.format_exc()))
                return self.valid_api

            self.valid_api = True
        
        except KeyError:
            self.valid_api = False
            logging.error('api entrega respuesta mal formada, codigo %s\n%s' % (self.code, traceback.format_exc()))

        return self.valid_api

    def request_URL(self):
        '''
        Hace el request a la URL asociada a la orden de compra y obtiene la informacion relevante
        '''
        self.justif = None
        self.notes = None
        self.anex = None
        self.dict_cm = {}
        self.dict_medida = {}
        self.dict_codigo_prod = {}  
        
        self.razon_social_comprador = None
        self.razon_social_facturacion = None
        self.razon_social_proveedor = None
        self.direccion_facturacion = None
        self.comuna_facturacion  = None
        self.direccion_envio_factura = None 
        self.metodo_despacho = None
        self.direccion_despacho = None
        self.fecha_entrega = None
        self.fax_contacto_comprador= None
        self.fax_contacto_proveedor= None
        self.nombre_proveedor_url = None
        self.forma_pago = None
        self.rut_facturacion = None
        self.gran_oc = None

        if self.url=='':
            return False
        
        try:
            self.urlInfo = requests.get(self.url).text
            self.urlsoup = BeautifulSoup(self.urlInfo,'html.parser')
        
        except:
            logging.error("error de apertura de la url, codigo %s\n%s" % (self.code, traceback.format_exc()))
            return False

        num=1
        bad=0
        #Obtencion de datos de codigo marco.
        while True:
            num+=1
            try:
                onu = self.urlsoup.find('span',{'id':'gv_ctl'+af.transform_num(num)+'_lnkViewLineData'})
                if onu is None:
                    break

                try:
                    medida=onu.parent.parent.next_sibling.next_sibling.next_sibling.get_text().strip()
                    self.dict_medida[(num-1)]=medida
                
                except:
                    pass

                if len(onu.contents)==0:
                    continue

                cod_onu=str(onu.contents[0]).strip()
                if 'CM' in self.code:
                    self.dict_cm[(num-1)]=str(onu.contents[1]).strip()[4:-5].strip().upper()

            except:
                logging.error("error en la obtencion de cm o medida, codigo %s\n%s" % (self.code, traceback.format_exc()))
                continue
        


        try:
            justifSoup = self.urlsoup.find('span',{'id':'lblJustificacionValue'})
            notesSoup = self.urlsoup.find('span',{'id':'lblNotesValue'})
            anexSoup = self.urlsoup.find('span',{'id':'lblResolutionValue'})
            
            #####
            self.razon_social_comprador = self.obtain_soup_text('lblBuyerSocialReasonValue')
            self.razon_social_facturacion = self.obtain_soup_text('lblReasonSocialValue')
            self.razon_social_proveedor = self.obtain_soup_text('lblSocialReasonValueP')
            self.direccion_facturacion = self.obtain_soup_text('lblDirectionValuePF')
            self.comuna_facturacion  = self.obtain_soup_text('lblCommuneValuePF')
            self.direccion_envio_factura = self.obtain_soup_text('lblDirectionInvoiseValue')
            self.metodo_despacho = self.obtain_soup_text('lblShipmentMethodValue')
            self.direccion_despacho = self.obtain_soup_text('lblDirectionValueD')
            self.fax_contacto_comprador = self.obtain_soup_text('lblFaxContactValueC')
            self.fax_contacto_proveedor = self.obtain_soup_text('lblFaxValueP')
            self.nombre_proveedor_url = self.obtain_soup_text('lblOrganizationValueP')
            self.forma_pago = self.obtain_soup_text('lblPaymentMethodValue')
            self.rut_facturacion =  self.obtain_soup_text('lblIDValue')

            if not justifSoup is None:
                self.justif=justifSoup.get_text()
            
            if not notesSoup is None:
                self.notes=notesSoup.get_text()
            
            if not anexSoup is None:
                if len(anexSoup.get_text()):
                    self.anex='1'
                else:
                    self.anex='0'

            fech=self.obtain_soup_text('lblDeliveryDateValue')
            if fech =='' or fech is None:
                self.fecha_entrega=None
            else:
                list_fecha=fech.split('-')
                list_fecha.reverse()                
                self.fecha_entrega='-'.join(list_fecha)

            #gran compra
            try:
                aux = self.obtain_soup_text('lblProvenienceValue')
                is_gran_oc = self.obtain_soup_text('lblProvenience')
                if 'GranCompra' in is_gran_oc:
                    self.gran_oc = aux
            except:
                self.gran_oc = None
            
        except:
            logging.error("error en la obtencion de info extra de url, codigo %s\n%s" % (self.code, traceback.format_exc()))
            return False

        
        return True



    def obtain_soup_text(self,ide):
        soup=self.urlsoup.find('span',{'id': ide})
        if soup is None:
            return None
        
        return soup.get_text().strip()

    def write_information(self):
        '''
        Luego de obtener la informacion relevante la pasa a la BD
        '''
        if not self.valid_api:
            self.write_error()
            return
        
        if not self.request_URL():
            self.write_error()

        info=self.jsonInfo['Listado'][0]
        fechas=info['Fechas']
        compr=info['Comprador']
        prov=info['Proveedor']
        tiene_items='0'
        cant_items=0
        
        if info['TieneItems']=='1':
            tiene_items='1'
            cant_items=info['Items']['Cantidad']

        params=[
            self.code,                                                  #codigo varchar(50)
            self.jsonInfo['FechaCreacion'],                             #fecha_descarga datetime2(0)
            info['CodigoEstado'],                                       #estado int
            fechas['FechaCreacion'],                                    #fecha_emision datetime2(0)
            fechas['FechaEnvio'],                                       #fecha_envio datetime2(0)
            fechas['FechaAceptacion'],                                  #fecha_aceptacion datetime2(0)
            fechas['FechaCancelacion'],                                 #fecha_cancelacion datetime2(0)
            fechas['FechaUltimaModificacion'],                          #fecha_ultima_mod datetime2(0)
            info['PromedioCalificacion'],                               #prom_calificacion numeric(3)
            info['CantidadEvaluacion'],                                 #cant_evaluacion int
            info['TipoDespacho'],                                       #tipo_despacho char(2)
            info['FormaPago'],                                          #tipo_pago char(2)
            info['TipoMoneda'],                                         #tipo_moneda char(10)
            info['Nombre'],                                             #nombre nvarchar(4000)
            self.anex,                                                  #anexos bit
            self.notes,                                                 #notas nvarchar(4000)
            info['Descripcion'],                                        #descripcion nvarchar(MAX)
            self.justif,                                                #proveniente_de nvarchar(1000)
            info['CodigoLicitacion'],                                   #proveniente_licitacion varchar(50)
            compr['CodigoOrganismo'],                                   #codigo_organismo_comprador varchar(50)
            compr['NombreOrganismo'],                                   #nombre_organismo_comprador nvarchar(4000)
            compr['CodigoUnidad'],                                      #codigo_unidad_comprador varchar(50)
            compr['NombreUnidad'],                                      #nombre_unidad_comprador nvarchar(4000)
            compr['Actividad'],                                         #actividad_comprador nvarchar(4000)
            self.razon_social_comprador,                                #razon_social_comprador nvarchar(4000)
            compr['RutUnidad'],                                         #rut_comprador char(15)
            compr['DireccionUnidad'],                                   #dir_unidad_comprador nvarchar(4000)
            compr['ComunaUnidad'],                                      #comuna_comprador nvarchar(4000)
            compr['RegionUnidad'],                                      #region_comprador nvarchar(4000)
            compr['Pais'],                                              #pais_comprador nvarchar(20)
            compr['NombreContacto'],                                    #nombre_contacto_comprador varchar(4000)
            compr['CargoContacto'],                                     #cargo_contacto_comprador nvarchar(4000)
            compr['FonoContacto'],                                      #fono_contacto_comprador nvarchar(50)
            self.fax_contacto_comprador,                                #fax_contacto_comprador nvarchar(1000)
            compr['MailContacto'],                                      #mail_contacto_comprador nvarchar(1000)
            self.forma_pago,                                            #forma_pago nvarcnar(1000)
            self.razon_social_facturacion,                              #razon_social_facturacion nvarchar(4000)
            self.rut_facturacion,
            self.direccion_facturacion,                                 #direccion_facturacion nvarchar(4000)
            self.comuna_facturacion,                                    #comuna_facturacion nvarchar(4000)
            self.direccion_envio_factura,                               #direccion_envio_factura nvarchar(4000)
            self.metodo_despacho,                                       #metodo_despacho nvarchar(4000)
            self.direccion_despacho,                                    #direccion_despacho nvarchar(4000)
            self.fecha_entrega,                                         #fecha_entrega datetime2(0)
            prov['Codigo'],                                             #codigo_proveedor varchar(50)
            prov['Nombre'],                                             #nombre_proveedor nvarchar(4000)
            self.nombre_proveedor_url,                                  #nombre_proveedor_url nvarchar(4000)
            prov['CodigoSucursal'],                                     #codigo_sucursal varchar(50)
            prov['NombreSucursal'],                                     #nombre_sucursal nvarchar(4000)
            info['CodigoEstadoProveedor'],                              #estado_proveedor int
            self.razon_social_proveedor,                                #razon_social_proveedor nvarchar(4000)
            prov['RutSucursal'],                                        #rut_proveedor char(15)
            prov['Direccion'],                                          #direccion_proveedor nvarchar(4000)
            prov['Comuna'],                                             #comuna_proveedor nvarchar(4000)
            prov['Region'],                                             #region_proveedor nvarchar(4000)
            prov['Pais'],                                               #pais_proveedor nvarchar(20)
            prov['NombreContacto'],                                     #contacto_proveedor nvarchar(4000)
            prov['CargoContacto'],                                      #cargo_contacto_proveedor nvarchar(4000)
            prov['FonoContacto'],                                       #fono_contacto_proveedor nvarchar(4000)
            self.fax_contacto_proveedor,                                #fax_contacto_proveedor nvarchar(4000)
            prov['MailContacto'],                                       #mail_contacto_proveedor nvarchar(4000)
            info['Impuestos'],                                          #impuesto int
            info['TotalNeto'],                                          #total_neto int
            info['Descuentos'],                                         #descuento int
            info['Cargos'],                                             #cargos int
            info['PorcentajeIva'],                                      #iva int
            info['Total'],                                              #total int
            tiene_items,                                                #tiene_items bit
            cant_items,                                                 #cant_items int
            self.url,                                                   #url varchar(50)
            self.gran_oc
            ]
        

        #insercion de ordenes
        if self.in_db:
            self.conn.update_orden(params)
        else:
            self.conn.insert_orden(params)
        state_params = [self.code, info['CodigoEstado']]
        #self.conn.insertState(state_params)
        
        #insercion de productos
        if tiene_items == '1':
            for prod in info['Items']['Listado']:
                try:
                    self.insert_product(prod)
                except:
                    logging.error("error en insercion de producto %s\n%s" % (self.code, traceback.format_exc()))

        return params

    def insert_product(self,prod):
        '''
        Inserta un producto de la orden de compra a la BD
        '''
        corr=prod['Correlativo']
        total_unit=0
        cod_prod=None
        
        if 'CM' in self.code:
            try:
                cod_prod=af.obtain_id_from_CM(prod['EspecificacionProveedor'])
            except:
                cod_prod='Invalido'
                logging.warning("error en la obtencion de codigo producto dada la specproveedor, codigo %s\n%s" % (self.code, traceback.format_exc()))
        try:
            total_unit=prod['Cantidad']*prod['PrecioNeto']
        
        except:
            total_unit=None
            logging.warning("error en obtencion de total_unit, codigo %s\n%s\n" % (self.code, traceback.format_exc()))

        params=[
            self.code,                                                  #orden_compra_codigo varchar(50)
            corr,                                                       #product_id int
            prod['CodigoProducto'],                                     #onu bigint
            self.dict_cm.get(corr),                                     #proveniente_cm varchar(50)
            prod['Producto'],                                           #producto_servicio nvarchar(4000)
            prod['Cantidad'],                                           #cantidad int
            self.dict_medida.get(corr),                                 #medida nvarchar(100)
            prod['EspecificacionComprador'],                            #esp_comprador nvarchar(4000)
            prod['EspecificacionProveedor'],                            #esp_proveedor nvarchar(4000)
            prod['PrecioNeto'],                                         #precio_unit int
            prod['TotalDescuentos'],                                    #descuentos int
            prod['TotalCargos'],                                        #cargos int
            prod['TotalImpuestos'],                                     #total_impuestos int
            total_unit,                                                 #total_unit int
            prod['Total'],                                              #valor_total int
            cod_prod                                                    #codigo_producto_cm
         ]

        if self.in_db:
            self.conn.update_product(params)
        else:
            self.conn.insert_product(params)
        return params


    def write_error(self):
        if not self.error:
            with open(self.output_name, "a") as myfile: myfile.write("%s\n" % self.code)
            self.error=True


class TraditionalParserOC:
    '''
    Hace el parseo de las ordenes de compra sin usar API
    '''

    max_tries = 4
    def __init__(self,url,code,name,conn, thread_id ,in_db=False):
        self.url = url
        self.code = code
        self.output_name = name
        self.conn = conn
        self.error = False
        self.in_db = in_db
        self.data = 0
        self.info = {}
        self.thread_id = thread_id
        self.anexo_null = False

    def request_URL(self):
        '''
        Request a la url en chilecompra y obtiene informacion relevante
        '''
        try:
            self.urlInfo = requests.get(self.url).text
            self.urlsoup = BeautifulSoup(self.urlInfo,'html.parser')
        
        except:
            logging.error("Thread: %d - error de apertura de la url, codigo %s\n%s" % (self.thread_id, self.code, traceback.format_exc()))
            return False
        
        try:
            info = {}
            info['estado_orden'] = self.obtain_soup_text('lblStatusPOValue')
            info['fecha_envio'] = self.obtain_soup_date('lblCreationDatePOValue')
            info['nombre_oc'] = self.obtain_soup_text('lblNamePOValue')
            info['notas'] = self.obtain_soup_text('lblNotesValue')
            info['justif'] = self.obtain_soup_text('lblJustificacionValue')
            info['justif'] = self.obtain_soup_text('lblJustificacionValue')
            info['unidad_compra_comprador'] = self.obtain_soup_text('lblBuyerOrganizationValue')
            info['razon_social_comprador'] = self.obtain_soup_text('lblBuyerSocialReasonValue')
            info['rut_comprador'] = self.obtain_soup_text('lblBuyerTaxIDValue')
            info['direccion_unidad_comprador'] = self.obtain_soup_text('lblDirectionBuyerValue')
            info['nombre_contacto'] = self.obtain_soup_text('lblContactValueC')
            info['cargo_contacto'] = self.obtain_soup_text('lblPositionContactValueC')
            info['telefono_contacto'] = self.obtain_soup_text('lblPhoneContactValueC')
            info['fax_contacto'] = self.obtain_soup_text('lblFaxContactValueC')
            
            info['forma_pago'] = self.obtain_soup_text('lblPaymentMethodValue')
            info['moneda'] = self.obtain_soup_text('lblPaymentTypeValue')
            info['razon_social_pago'] = self.obtain_soup_text('lblReasonSocialValue')
            info['rut_pago'] = self.obtain_soup_text('lblIDValue')
            info['direccion_facturacion'] = self.obtain_soup_text('lblDirectionValuePF')
            info['comuna_pago'] = self.obtain_soup_text('lblCommuneValuePF')
            info['impuesto_pago'] = af.money_to_decimal(self.obtain_soup_text('lblTaxValue'))
            info['direccion_envio_factura'] = self.obtain_soup_text('lblDirectionInvoiseValue')
            info['metodo_despacho'] = self.obtain_soup_text('lblShipmentMethodValue')
            info['direccion_despacho'] = self.obtain_soup_text('lblDirectionValueD')
            info['fecha_entrega'] = self.obtain_soup_date('lblDeliveryDateValue')
            

            info['proveedor'] = self.obtain_soup_text('lblOrganizationValueP')
            info['razon_social_proveedor'] = self.obtain_soup_text('lblSocialReasonValueP')
            info['rut_proveedor'] = self.obtain_soup_text('lblRutValueP')
            info['sucursal_proveedor'] = self.obtain_soup_text('lblBranchValue')
            info['direccion_proveedor'] = self.obtain_soup_text('lblDirectionValueP')
            info['comuna_proveedor'] = self.obtain_soup_text('lblCommuneValueP')
            info['contacto_proveedor'] = self.obtain_soup_text('lblContactValueP')
            info['cargo_proveedor'] = self.obtain_soup_text('lblPositionContactValueP')
            info['telefono_proveedor'] = self.obtain_soup_text('lblPhoneValueP')
            info['fax_proveedor'] = self.obtain_soup_text('lblFaxValueP')

            info['total_neto'] = af.money_to_decimal(self.obtain_soup_text('lblAmountShow'))
            info['descuento'] = af.money_to_decimal(self.obtain_soup_text('lblDiscountShow'))
            info['cargos'] = af.money_to_decimal(self.obtain_soup_text('lblChargesShow'))
            info['iva'] = af.money_to_decimal(self.obtain_soup_text('lblTaxesShow'))
            info['iva_porc'] = af.money_to_decimal(self.obtain_soup_text('lblNumberPercent'))
            info['impuesto_especifico'] = af.money_to_decimal(self.obtain_soup_text('lblImpuestoEspShow'))
            info['total'] = af.money_to_decimal(self.obtain_soup_text('lblTotalAmountShow'))



            #anexos
            anexo_text = self.obtain_soup_text('lblResolutionValue')
            if anexo_text is None:
                info['anexos'] = None
                self.anexo_null = True
                
            elif len(anexo_text):
                info['anexos'] = '1'
            
            else:
                info['anexos'] = '0'

            #gran compra
            gran_oc = None
            try:
                aux = self.obtain_soup_text('lblProvenienceValue')
                is_gran_oc = self.obtain_soup_text('lblProvenience')
                if 'GranCompra' in is_gran_oc:
                    gran_oc = aux
                else:
                    info['proveniente_licitacion'] = aux
            except:
                gran_oc = None
            info['gran_oc'] = gran_oc

            #mails
            soup_email = self.urlsoup.find('img',{'id':'imgMailContactValueC'})
            if soup_email is None:
                info['email_contacto'] = None
            else:
                info['email_contacto'] = 'http://www.mercadopublico.cl' + soup_email['src'][8:]

            soup_email = self.urlsoup.find('img',{'id':'imgMailContactValueP'})
            if soup_email is None:
                info['email_proveedor'] = None
            else:
                info['email_proveedor'] = 'http://www.mercadopublico.cl' + soup_email['src'][8:]

            self.info = info
            return self.get_products_info()


        except:
            logging.error("Thread: %d - error en la obtencion de info de url, codigo %s\n%s" % (self.thread_id, self.code, traceback.format_exc()))
            return False

    def get_products_info(self):
        '''
        Obtiene la informacion relacionada con productos de una orden de compra
        '''
        self.products_info = []
        resp = True
        num=1
        while True:
            num+=1
            try:
                prod = {}
                onu = self.urlsoup.find('span',{'id':'gv_ctl'+af.transform_num(num)+'_lnkViewLineData'})
                if onu is None:
                    break
                
                soup_prod = onu.parent.parent
                prod['corr'] = num - 1
                prod['producto_servicio'] = self.get_sibling_text(soup_prod, 1)
                prod['cantidad'] = af.money_to_decimal(self.get_sibling_text(soup_prod, 2))
                prod['medida'] = self.get_sibling_text(soup_prod, 3)
                prod['esp_comprador'] = self.get_sibling_text(soup_prod, 4)
                prod['esp_proveedor'] = self.get_sibling_text(soup_prod, 5)
                prod['precio_unit'] = af.money_to_decimal(self.get_sibling_text(soup_prod, 6))
                prod['descuentos'] = af.money_to_decimal(self.get_sibling_text(soup_prod, 7))
                prod['cargos'] = af.money_to_decimal(self.get_sibling_text(soup_prod, 8))
                prod['total_unit'] = af.money_to_decimal(self.get_sibling_text(soup_prod, 9))
                prod['valor_total'] = af.money_to_decimal(self.get_sibling_text(soup_prod, 10))


                if len(onu.contents)==0:
                    self.products_info.append(prod)
                    continue

                prod['onu']=str(onu.contents[0]).strip()
                if 'CM' in self.code:
                    prod['cod_marco']=str(onu.contents[1]).strip()[4:-5].strip().upper()

                self.products_info.append(prod)
                

            except:
                logging.error("Thread: %d - error en la obtencion de info de productos, codigo %s\n%s" % (self.thread_id, self.code, traceback.format_exc()))
                resp = False
                continue
        return resp

    def write_information(self):
        '''
        Escribe la informacion a la BD
        '''
        if not self.request_URL() or self.anexo_null:
            self.write_error()


        tiene_items='0'
        cant_items=len(self.products_info)
        
        if cant_items > 0:
            tiene_items = '1'

        params=[
            self.code,              # id    varchar(50) Unchecked
            self.info.get('estado_orden'),              # estado    nvarchar(50)    Checked
            datetime.datetime.now(),                # fecha_descarga    datetime2(7)    Checked
            self.info.get('fecha_envio'),               # fecha_envio   datetime2(7)    Checked
            self.info.get('nombre_oc'),             # nombre    nvarchar(4000)  Checked
            None,               # descripcion   nvarchar(MAX)   Checked
            self.info.get('moneda'),                # moneda    nvarchar(50)    Checked
            self.info.get('anexos'),                #anexos bit Checked
            self.info.get('notas'),             # notas nvarchar(4000)  Checked
            self.info.get('justif'),                # proveniente_de    nvarchar(1000)  Checked
            self.info.get('proveniente_licitacion'),                # proveniente_licitacion    varchar(50) Checked
            self.info.get('razon_social_comprador'),                # nombre_organismo_comprador    nvarchar(4000)  Checked
            self.info.get('unidad_compra_comprador'),               # nombre_unidad_comprador   nvarchar(4000)  Checked
            self.info.get('rut_comprador'),             # rut_comprador char(15)    Checked
            None,               # region_comprador  nvarchar(4000)  Checked
            None,               # comuna_comprador  nvarchar(4000)  Checked
            self.info.get('direccion_unidad_comprador'),                # dir_unidad_comprador  nvarchar(4000)  Checked
            self.info.get('nombre_contacto'),               # nombre_contacto_comprador nvarchar(4000)  Checked
            self.info.get('cargo_contacto'),               # cargo_contacto_comprador  nvarchar(4000)  Checked
            self.info.get('telefono_contacto'),             # fono_contacto_comprador   nvarchar(50)    Checked
            self.info.get('fax_contacto'),              # fax_contacto_comprador    nvarchar(1000)  Checked
            self.info.get('email_contacto'),                # mail_contacto_comprador   nvarchar(1000)  Checked
            self.info.get('forma_pago'),                # forma_pago    nvarchar(1000)  Checked
            self.info.get('razon_social_pago'),             # razon_social_facturacion  nvarchar(4000)  Checked
            self.info.get('rut_pago'),              # rut_facturacion   char(15)    Checked
            self.info.get('direccion_facturacion'),             # direccion_facturacion nvarchar(4000)  Checked
            self.info.get('comuna_pago'),               # comuna_facturacion    nvarchar(4000)  Checked
            self.info.get('direccion_envio_factura'),               # direccion_envio_factura   nvarchar(4000)  Checked
            self.info.get('metodo_despacho'),               # metodo_despacho   nvarchar(4000)  Checked
            self.info.get('direccion_despacho'),                # direccion_despacho    nvarchar(4000)  Checked
            self.info.get('fecha_entrega'),             # fecha_entrega datetime2(0)    Checked
            self.info.get('proveedor'),             # nombre_proveedor  nvarchar(4000)  Checked
            self.info.get('razon_social_proveedor'),                # razon_social_proveedor    nvarchar(4000)  Checked
            self.info.get('sucursal_proveedor'),                # nombre_sucursal   nvarchar(4000)  Checked
            self.info.get('rut_proveedor'),             # rut_proveedor char(15)    Checked
            self.info.get('direccion_proveedor'),               # direccion_proveedor   nvarchar(4000)  Checked
            None,               # region_proveedor  nvarchar(4000)  Checked
            self.info.get('comuna_proveedor'),              # comuna_proveedor  nvarchar(4000)  Checked
            self.info.get('contacto_proveedor'),                # contacto_proveedor    nvarchar(4000)  Checked
            self.info.get('cargo_proveedor'),               # cargo_contacto_proveedor  nvarchar(4000)  Checked
            self.info.get('telefono_proveedor'),                # fono_contacto_proveedor   nvarchar(4000)  Checked
            self.info.get('fax_proveedor'),             # fax_contacto_proveedor    nvarchar(4000)  Checked
            self.info.get('email_proveedor'),               # mail_contacto_proveedor   nvarchar(4000)  Checked
            self.info.get('total_neto'),                # total_neto    numeric(30, 3)  Checked
            self.info.get('descuento'),             # descuento numeric(30, 3)  Checked
            self.info.get('cargos'),                # cargos    numeric(30, 3)  Checked
            self.info.get('iva_porc'),              # iva_porcentaje    int Checked
            self.info.get('iva'),               # iva   numeric(30, 3)  Checked
            self.info.get('impuesto_pago'),             # impuesto  numeric(30, 3)  Checked
            self.info.get('total'),             # total numeric(30, 3)  Checked
            tiene_items,                # tiene_items   bit Checked
            cant_items,             # cant_items    numeric(30, 3)  Checked
            self.url,               # url   nvarchar(1000)  Checked
            self.info.get('gran_oc'),               # gran_compra   nvarchar(1000)  Checked
            af.state_to_integer(self.info.get('estado_orden'))  #estado_int int Checked
            ]
        

        #insercion de ordenes
        if self.in_db:
            self.conn.update_orden(params)
        else:
            self.conn.insert_orden(params)
    
        #insercion de productos
        for prod in self.products_info:
            self.insert_product(prod)
        return params

    def insert_product(self, prod):
        '''
        Inserta producto en la BD
        '''
        corr = prod['corr']
        cod_prod = None
        
        if 'CM' in self.code:
            try:
                cod_prod=af.obtain_id_from_CM(prod['esp_proveedor'])
            except:
                cod_prod='Invalido'
                logging.warning("error en la obtencion de codigo producto dada la specproveedor, codigo %s\n%s" % (self.code, traceback.format_exc()))

        params=[
            self.code,                                                  #orden_compra_id varchar(50)
            prod['corr'],                                               #product_id int
            prod.get('onu'),                                        #onu bigint
            prod.get('cod_marco'),                                      #proveniente_cm varchar(50)
            prod.get('producto_servicio'),                                          #producto_servicio nvarchar(4000)
            prod.get('cantidad'),                                           #cantidad int
            prod.get('medida'),                             #medida nvarchar(100)
            prod.get('esp_comprador'),                          #esp_comprador nvarchar(4000)
            prod.get('esp_proveedor'),                          #esp_proveedor nvarchar(4000)
            prod.get('precio_unit'),                                            #precio_unit int
            prod.get('descuentos'),                                 #descuentos int
            prod.get('cargos'),                                     #cargos int
            None,                                       #total_impuestos int
            prod.get('total_unit'),                                                 #total_unit int
            prod.get('valor_total'),                        #valor_total int
            cod_prod    #codigo_producto_cm
         ]

        #print params

        if self.in_db:
            self.conn.update_product(params)
        else:
            self.conn.insert_product(params)
        return params


    
    def get_sibling_text(self, soup, num):
        i = 0
        while i < num:
            soup = soup.next_sibling
            i+=1
        return soup.get_text().strip()


    def obtain_soup_date(self, ide):
        fech = self.obtain_soup_text(ide)
        if fech =='' or fech is None:
            return None
        else:
            list_fecha = fech.split('-')
            list_fecha.reverse()                
            resp = '-'.join(list_fecha)
            return datetime.datetime.strptime(resp, '%Y-%m-%d')
    
    
    def obtain_soup_text(self, ide):
        soup=self.urlsoup.find('span',{'id': ide})
        if soup is None:
            return None
        
        return soup.get_text().strip()


    def write_error(self):
        if not self.error:
            with open(self.output_name, "a") as myfile: myfile.write("%s\n" % self.code)
            self.error=True