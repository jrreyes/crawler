import httplib 
import requests
from bs4 import BeautifulSoup
import re
import datetime as dt
import auxiliar as af
import database as db
from parserOC import ParserOC, TraditionalParserOC
import traceback
import logging
import ConfigParser
import time

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait 
from selenium.webdriver.support import expected_conditions as EC


class Obtainer:
    '''
    Clase que hace la obtencion de datos:
    '''
    def __init__(self, ide):
        self.api_ticket = ''
        self.url_busq_oc = 'http://www.mercadopublico.cl/Portal/Modules/Site/Busquedas/BuscadorAvanzado.aspx?qs=2'
        self.always_try = False
        self.never_try = False
        self.max_tries = 2
        self.tickets = []
        self.connect()
        self.thread_id = ide
        # driver de selenium
        self.driver = webdriver.PhantomJS('phantomjs-2.1.1-macosx/bin/phantomjs', service_args=['--ignore-ssl-errors=true'])
        self.driver.implicitly_wait(10)
        

    def define_tickets(self, tickets):
        self.tickets = tickets        

    def create_log(self,path):
        with open(path+'.log', 'w'):
            pass
        logging.basicConfig(filename=path+'.log', level=logging.INFO)
        logging.info('Started')

    def redefine_driver(self):
        self.driver.close()
        self.driver = webdriver.PhantomJS('phantomjs-2.1.1-macosx/bin/phantomjs', service_args=['--ignore-ssl-errors=true'])
        self.driver.implicitly_wait(10)
    
    def connect(self):
        self.conn = db.Connection()

    
    def obtain_info_oc(self,in_oc,out_oc):
        '''
        Obtencion de ordenes de compra a partir de un archivo .txt, esto se hace leyendo un archivo y
        parseando la informacion de la pagina
        '''
        #Ordenes de compras segun txt
        i=0
        t=0
        size=len(self.tickets)
        self.api_ticket=self.tickets[0]
        self.create_log(out_oc)
        with open(in_oc) as f:
            for line in f:
                logging.info("Thread: %d, %d" % (self.thread_id, i))
                #print i
                i+=1
                p=None
                try:
                    code=line.strip()
                    print ("T: %d, Num: %d\n" % (self.thread_id, i))
                    url=self.conn.in_database_OC(code)
                    in_db=False

                    if url is None or url.strip() == '':
                        url = self.request_OC_link(code,'Codigo: '+code+' no pudo obtener informacion del crawler')
                    if not url.strip() == '':
                        p = TraditionalParserOC(url, code, out_oc, self.conn, self.thread_id, in_db = in_db)
                        p.write_information()
                    else:
                        logging.error("Thread: %d error inesperado codigo %s, no se pudo encontrar url\n" % (self.thread_id, code))

                except:
                    if p is None or not p.error:
                        with open(out_oc, "a") as myfile: myfile.write("%s\n"%line.strip())
                    logging.error("Thread: %d error inesperado codigo %s\n%s" % (self.thread_id, line.strip(), traceback.format_exc()))



    def obtain_info_oc_api(self,in_oc,out_oc):
        '''
        Obtencion de ordenes de compra a partir de un archivo .txt, esto se hace leyendo un archivo y
        haciendo consultas a la api (usar para obtener contenido historico)
        '''
        #Ordenes de compras segun txt
        i=0
        t=0
        size=len(self.tickets)
        self.api_ticket=self.tickets[0]
        self.create_log(out_oc)
        with open(in_oc) as f:
            for line in f:
                logging.info("Thread: %d, %d" % (self.thread_id, i))
                i+=1
                p=None
                try:
                    code=line.strip()
                    print ("T: %d, Num: %d\n" % (self.thread_id, i)),
                    url=self.conn.in_database_OC(code)
                    in_db=False
                    
                    if url is None or url.strip() == '':
                        url = self.request_OC_link(code,'Codigo: '+code+' no pudo obtener informacion del crawler')
                    while True:
                        api='http://api.mercadopublico.cl/servicios/v1/publico/ordenesdecompra.json?codigo='+code+'&ticket='+self.api_ticket
                        p=ParserOC(url,api,code,out_oc,self.conn,in_db=in_db)
                        p.request_API()
                        if p.ticket_overflow:
                            t=(t+1)%size
                            self.api_ticket=self.tickets[t]
                            continue
                        p.write_information()
                        break
                
                except:
                    if p is None or not p.error:
                        with open(out_oc, "a") as myfile: myfile.write("%s\n" % line.strip())
                    logging.error("Thread: %d error inesperado codigo %s\n%s" % (self.thread_id, line.strip(), traceback.format_exc())) 

    def actualize_oc(self,out_oc,start_date,end_date):
        '''
        Obtencion de codigos de ordenes de compra indicando fechas de inicio y termino, esta consulta a la api
        por lo cambios en ordenes de compra dadas esas fechas.
        '''
        total_days = (end_date - start_date).days + 1
        self.create_log(out_oc)
        self.api_ticket = self.tickets[0]
        with open(out_oc,"w") as outf:

            for day_number in range(total_days):
                current_date = (start_date + dt.timedelta(days = day_number)).date()
                date = af.change_date_format(current_date)
                url_date ='http://api.mercadopublico.cl/servicios/v1/publico/ordenesdecompra.json?fecha='+date+'&ticket='+self.api_ticket
                time.sleep(2)
                print url_date
                mess = 'Fecha: '+date+' no pudo obtener info de la API para ordenes de compra'
                req_date = self.request_smthg(url_date,mess)
                
                #si no se pudo obtener, seguir
                if req_date == None:
                    logging.error("No se pudo obtener info de esa fecha%s" % (date.strip()))
                    continue
                
                try:
                    total = req_date.json()
                    cant = total['Cantidad']
                
                except:
                    logging.error("Fecha no valida %s\n%s" % (date.strip(), traceback.format_exc()))
                    continue
            
                i = 0
                print len(total['Listado'])
                for cod in total['Listado']:
                    i+=1
                    p=None
                    code=cod['Codigo']
                    outf.write("%s\n"%code)

    ####################################
    ####### Funciones Auxiliares #######
    ####################################

    def find_OC_link_selenium(self,code):
        
        '''
        Encuentra el link asociado a un codigo en particular de una orden de compra con selenium
        '''
        self.driver.get("http://www.mercadopublico.cl/Portal/Modules/Site/Busquedas/BuscadorAvanzado.aspx?qs=2")
        self.driver.find_element_by_id("txtSearch").send_keys(code)
        self.driver.find_element_by_id("btnBusqueda").click()
        WebDriverWait(self.driver, 10).until(EC.title_is("Mercado Publico"))

        links = self.driver.find_elements_by_id("rptResultados_ctl01_lnkCodigo")

        if (links is None) or (len(links) == 0):
            return ''

        link = links[0].get_attribute("onclick")
        start='javascript:window.open(\'../../../../'
        end='\',\'Detail\',\'width' 
        first_index=link.find(start)+len(start)
        last_index=link.find(end)
        link=link[first_index:last_index]


        return 'http://www.mercadopublico.cl/'+link


    def request_smthg(self, url, mess):
        '''
        Permite hacer request mas perzonalizado
        '''
        keep_trying = True
        out = None
        tries = 0
        while keep_trying:
            try:
                tries += 1
                out = requests.get(url)
                break
            
            except:
                if self.never_try or tries > self.max_tries:
                    keep_trying=False
                    print mess+', no se seguira intentando'
                    logging.error("%s, no se seguira intentando, url %s\n%s" % (mess, url, traceback.format_exc()))
                    break

                if self.always_try:
                    print mess+', se seguira intentando'
                    logging.error("%s, se seguira intentando, url %s\n%s" % (mess, url, traceback.format_exc()))
                    break

        return out


    def request_OC_link(self,code, mess):
        '''
        Permite hacer el request para encontrar el link de un cm
        '''
        keep_trying = True
        url = ''
        tries = 0
        
        while keep_trying:
            try:
                tries += 1
                url = self.find_OC_link_selenium(code)
                if tries > self.max_tries:
                    logging.error("%s, no se seguira intentando, codigo %s\n%s" % (mess, code, traceback.format_exc()))
                    break
                if url == '':
                    continue
                break
            
            except:
                print 'timeout'
                logging.error("%s, timeout codigo %s\n%s" % (mess, code, traceback.format_exc()))
                self.redefine_driver()
                if self.never_try or tries > self.maxTries:
                    keep_trying=False
                    print mess+', no se seguira intentando'
                    logging.error("%s, no se seguira intentando, codigo %s\n%s" % (mess, code, traceback.format_exc()))

        return url
