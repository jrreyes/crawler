import re
import datetime as dt
from collections import OrderedDict
import database as db

'''
Metodos Auxiliares
'''

def transform_num(num):
    '''
    permite transforman un numero a formato de 2 digitos o mas (Formato comun para consultar en la api)
    '''
    if num>=0 and num<10:
        return '0'+str(num)
    return str(num)


def string_to_date(date_text):
    '''
    Transforma un string a Date
    '''
    listDate=date_text.split('-')
    return dt.datetime(int(listDate[2]), int(listDate[1]), int(listDate[0]))


def change_date_format(date):
    '''
    Transforma fecha a una aceptada por la api
    ''' 
    return transform_num(date.day)+transform_num(date.month)+str(date.year)

def obtain_id_from_CM(descr):
    '''
    Obtiene un id asociado dada la descripcion del producto
    '''
    m = re.search('\((.+?)\)', descr)
    found=''
    if m:
        found = m.group(1)
    return found

def repetition_remover(path_in, path_out):
    uniqlines=list(OrderedDict.fromkeys(list(open(path_in).readlines())))
    bar = open(path_out, 'w')
    bar.writelines(uniqlines)
    bar.close()


def divide(path_in, path_out, num):
    uniqlines = list(open(path_in).readlines())
    chunk_size = -(-len(uniqlines) // num)
    if chunk_size == 0:
        return

    new_lines = chunks(uniqlines, chunk_size)

    i = 1
    for chunk in new_lines:
        with open('%s%d.txt'%(path_out,i),'w') as f:
            for line in chunk:
                f.write('%s'%line)
        i += 1

def chunks(l, n):
    for i in xrange(0, len(l), n):
        yield l[i:i+n]

def slice_list(l, size):
    input_size = len(l)
    slice_size = input_size / size
    remain = input_size % size
    result = []
    iterator = iter(l)
    for i in range(size):
        result.append([])
        for j in range(slice_size):
            result[i].append(iterator.next())
        if remain:
            result[i].append(iterator.next())
            remain -= 1
    return result


def find_missing(path_in, path_out):
    '''
    dado un archivo de input con ids de orden de compra
    se encuentran todos los codigos que no estan en la base de datos pero
    si en el archivo y se retornan.
    '''
    conn=db.Connection()
    out=open(path_out, 'w')
    try:
        with open(path_in) as inp:
            i=0
            for line in inp:
                arr = line.split()
                code = arr[0].strip()
                if conn.in_database_OC(code) is None:
                    aux=line+'\n'
                    out.write(line)
                else:
                    i+=1
            print i
    finally:
        out.close()


def money_to_decimal(money):
    '''
    transforma formato de dinero de las ordenes de compra a money_to_decimal
    '''
    if money is None:
        return None
    money = money.replace(".", "")
    money = money.replace(",", ".")
    numbs = re.findall("\d+\.\d+", money)
    if len(numbs):
        return numbs[0]
    numbs = re.findall(r'\d+', money)
    if len(numbs):
        return numbs[0]
    return None

def state_to_integer(state):
    '''
    transforma un estado a su representacion en codigo
    '''
    if 'proveedor' in state:
        return 4
    if 'Aceptada' == state:
        return 6
    if 'Cancelada' == state:
        return 9
    if 'Incompleta' in state:
        return 15
    if 'En proceso' == state:
        return 5
    if 'Conforme' in state:
        return 12
    if 'Recepcionar' in state:
        return 13
    if 'Parcialmente' in state:
        return 14

    return None
