orden_compra_codigo	varchar(50)	Unchecked
product_id	int	Unchecked
onu	bigint	Checked
proveniente_cm	varchar(50)	Checked
producto_servicio	nvarchar(MAX)	Checked
cantidad	numeric(30, 3)	Checked
medida	nvarchar(100)	Checked
esp_comprador	nvarchar(MAX)	Checked
esp_proveedor	nvarchar(MAX)	Checked
precio_unit	numeric(30, 3)	Checked
descuentos	numeric(30, 3)	Checked
cargos_2	numeric(30, 3)	Checked
total_impuestos	numeric(30, 3)	Checked
total_unit	numeric(30, 3)	Checked
valor_total	numeric(30, 3)	Checked
codigo_producto_cm	varchar(50)	Checked