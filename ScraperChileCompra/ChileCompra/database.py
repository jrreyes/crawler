import pyodbc
import logging
import ConfigParser

'''
Representa la conexion con la base de datos
Contiene multiples metodos para hacer distintas consultas
'''


class Connection:
    def __init__(self):
        config = ConfigParser.ConfigParser()
        config.read('config.ini')
        type_orden = config.getint('OC-Obtainer', 'Type')
        print 'Conectando a la base...'

        self._cnxn = pyodbc.connect('Trusted_Connection=yes', driver=config.get('Database - OC', 'Driver'),
                                    server=config.get('Database - OC', 'Server'), port=config.get('Database - OC', 'Port'),
                                    database=config.get('Database - OC', 'Database'), uid=config.get('Database - OC', 'Uid'),
                                    pwd=config.get('Database - OC', 'Pwd'))

        self._cursor = self._cnxn.cursor()
        
        if type_orden == 0:
            self._order_query = self.generate_insert_query('files/schema/orden_traditional.txt','orden_compra')
            self._order_update_query = self.generate_update_query('files/schema/orden_traditional.txt','orden_compra',['id'])
        else:
            self._order_query = self.generate_insert_query('files/schema/mail.txt','orden_compra')
            self._order_update_query = self.generate_update_query('files/schema/mail.txt','orden_compra',['id'])

        self._product_query = self.generate_insert_query('files/schema/productos.txt','productos')
        self._product_update_query = self.generate_update_query('files/schema/productos.txt','productos',['orden_compra_id','product_id'])

        print 'Conexion lista'

    def generate_update_query(self,schema,table,ids):
        '''
        Genera query de update para ordenes de compra
        '''
        query='update '+table+' '
        set_list=[]
        where_list=[]
        with open(schema) as sch:
            for line in sch:
                col=line.split()[0].strip()
                set_list.append("%s=?" % col )
        
        for ide in ids:
            where_list.append('%s=?'%ide)

        sets=','.join(set_list)
        wheres=' and '.join(where_list)

        return 'update %s set %s where %s' % (table,sets,wheres)

    def generate_insert_query(self,schema,table):
        '''
        Genera query de insert para ordenes de compra
        '''
        query='insert into '+table
        columns_list=[]
        values_list=[]
        
        with open(schema) as sch:
            for line in sch:
                col=line.split()[0].strip()
                columns_list.append(col)
                values_list.append('?')

        columns='(%s)' % ','.join(columns_list)
        values='(%s)' % ','.join(values_list)
        
        return 'insert into %s%s values %s' % (table,columns,values)

    def in_database_OC(self,code):
        '''
        Revisa si es que una orden de compra se encuentra o no en la BD
        '''
        self._cursor.execute('select url from orden_compra where codigo = ?', code)
        row = self._cursor.fetchone()
        if row:
            return row.url
        return None

    #Esto no salva race conditions, pueden haber problemas de concurrencia pero son riesgos muy bajos ya que suponemos que no habran deletes
    def insert_orden(self,params):
        '''
        Hace la insercion de orden de compra en la BD, si ya esta intentara un update
        '''
        try:
            self._cursor.execute(self._order_query,params)
            self._cnxn.commit()
            logging.info('insert correcto de orden %s' % params[0])

        except pyodbc.IntegrityError:
            logging.warning('orden %s ya esta en la base, se hara un update' % params[0])
            params.append(params[0])
            self._cursor.execute(self._order_update_query,params)
            self._cnxn.commit()
            logging.info('update correcto de orden %s' % params[0])

    
    def insert_product(self,params):
        '''
        Hace la insercion de un producto de cierta orden de compra en la BD, si ya esta intentara un update
        '''
        try:
            self._cursor.execute(self._product_query,params)
            self._cnxn.commit()
            logging.info('insert correcto de producto %d de orden %s' % (params[1],params[0]))
        
        except pyodbc.IntegrityError:
            logging.warning('producto %d de orden %s ya esta en la base, se hara un update' % (params[1],params[0]))
            params.append(params[0])
            params.append(params[1])
            self._cursor.execute(self._product_update_query,params)
            self._cnxn.commit()
            logging.info('update correcto de producto %d de orden %s' % (params[1],params[0]))

    def update_orden(self,params):
        param_upd=params[:]
        param_upd.append(params[0])
        num_affected=self._cursor.execute(self._order_update_query,param_upd).rowcount
        self._cnxn.commit()
        
        if num_affected==0:
            logging.warning('orden %s no esta en la base, se hara un insert' % params[0])
            self._cursor.execute(self._order_query,params)
            self._cnxn.commit()
            logging.info('insert correcto de orden %s' % params[0])
            return
        
        logging.info('update correcto de orden %s' % params[0])


    
    def update_product(self,params):
        param_upd=params[:]
        param_upd.append(params[0])
        param_upd.append(params[1])
        num_dffected=self._cursor.execute(self._product_update_query,param_upd).rowcount
        self._cnxn.commit()

        if num_affected == 0:
            logging.warning('producto %d de orden %s no esta en la base, se hara un insert' % (params[1],params[0]))
            self._cursor.execute(self._product_query,params)
            self._cnxn.commit()
            logging.info('insert correcto de producto %d de orden %s' % (params[1],params[0]))
            return
        
        logging.info('update correcto de producto %d de orden %s' % (params[1],params[0]))


    def close(self):
        self._cnxn.close()
        self._cnxn_states.close()
