import crawler as ca
import threading
import ConfigParser
import auxiliar
import logging
import sys
import traceback

config = ConfigParser.ConfigParser()
config.read('config.ini')
threads = config.getint('Application', 'Threads')
date1 = config.get('DateObtainer', 'Date1')
date2 = config.get('DateObtainer', 'Date2')
output_date = config.get('DateObtainer', 'Output')
input_obtainer = config.get('OC-Obtainer', 'Input')
input_crawler = config.get('Crawler', 'Input')
out_crawler = config.get('Crawler', 'Output')
buffer_missing = config.get('Missing', 'Buffer')
out_missing = config.get('Missing', 'Output')
tickets_path = config.get('Crawler', 'Tickets')
type_orden = config.getint('OC-Obtainer', 'Type')


def obtain_oc(ide):
    '''
    Obtiene informacion de ordenes de compra de forma tradicional
    '''
    obt = ca.Obtainer(ide)
    tickets = gen_tickets()
    obt.define_tickets(tickets[ide-1])
    inp = '%s%d.txt' % (input_crawler, ide)
    outp = '%s%d.txt' % (out_crawler, ide)
    obt.obtain_info_oc(inp, outp)

def obtain_oc_api(ide):
    '''
    Obtiene informacion de ordenes de compra usando api
    '''
    obt = ca.Obtainer(ide)
    tickets = gen_tickets()
    obt.define_tickets(tickets[ide-1])
    inp = '%s%d.txt' % (input_crawler, ide)
    outp = '%s%d.txt' % (out_crawler, ide)
    obt.obtain_info_oc_api(inp, outp)

def divide_input():
    auxiliar.divide(input_obtainer, input_crawler, threads)

def create_files(path_file):

    for i in xrange(threads):
        path = '%s%d.txt' % (path_file, (i+1))
        created_file = open(path, 'w')
        created_file.close()

def obtain_dates():
    '''
    Obtiene los codigos de ordenes de compra dada una fecha de inicio y termino
    '''
    obt = ca.Obtainer(1)
    tickets = gen_tickets()
    obt.define_tickets(tickets[0])
    obt.actualize_oc(output_date, auxiliar.string_to_date(date1), auxiliar.string_to_date(date2))

def pregenerate():
    '''
    Crea los archivos necesarios segun la cantidad de threads antes de empezar el scraping
    '''
    create_files(input_crawler)
    create_files(out_crawler)
    divide_input()


def get_missing():
    '''
    Encuentra todos los codigos faltantes que estan en el los archivos pero que no estan en la base de datos
    '''
    print 'Buscando codigos faltantes'
    auxiliar.find_missing(input_obtainer, buffer_missing)

    with open(buffer_missing, 'a') as bm:
        for i in xrange(threads):
            path = '%s%d.txt' % (out_crawler, (i+1))
            out_file = open(path)
            for line in out_file:
                bm.write("%s\n" % line.strip()) 
                
    auxiliar.repetition_remover(buffer_missing, out_missing)
    print 'Terminada busqueda'

def gen_tickets():
    '''
    Separa los tickets segun la cantidad de threads para que cada instancia de la aplicacion tenga un ticket asociado
    '''
    tickets = []
    resp = []   
    with open(tickets_path) as tick:
        for t in tick:
            tickets.append(t.strip())


    cant_tick = len(tickets)

    if cant_tick >= threads:
        resp = auxiliar.slice_list(tickets, threads)      

    else:
        resp = []
        id_list = auxiliar.slice_list(xrange(threads), cant_tick)
        id_sets = [set(x) for x in id_list]
        for t in xrange(threads):
            i = 0
            current = []
            for s in id_sets:
                if t in s:
                    current.append(tickets[i])
                    break
                i+=1
            resp.append(current)

    for t in xrange(threads):
        print t, resp[t]

    return resp

def create_log(path):
    '''
    Crea log de la aplicacion
    '''
    with open(path+'.log', 'w'):
        pass
    logging.basicConfig(filename=path+'.log', level=logging.INFO)
    logging.info('Started')

if __name__ == '__main__':
    try:
        arg = sys.argv[1]
        if arg == 'missing':
            get_missing()
            input_obtainer = out_missing
            pregenerate()
        
        elif arg == 'preg':
            pregenerate()

        elif arg == 'date':
            obtain_dates()
            pregenerate()
                    
        elif arg == 'run':
            ide = int(sys.argv[2])
            obtain_oc(ide)

        else:
            print 'Escoga un argumento..'
    except:
        print 'Error en argumentos'
        print traceback.format_exc()
        aux = input()
        
