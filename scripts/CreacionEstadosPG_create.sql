-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2017-04-29 18:47:38.756

-- tables
-- Table: orden_compra
CREATE TABLE orden_compra (
    id serial  NOT NULL,
    codigo varchar(40) NOT NULL
    nombre varchar(4000)  NULL,
    estado varchar(50)  NULL,
    estado_int int  NULL,
    CONSTRAINT PK_orden_compra PRIMARY KEY (id, codigo)
);

-- End of file.

