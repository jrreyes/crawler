-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2017-04-29 18:37:27.776

-- tables
-- Table: estados
CREATE TABLE estados (
    id serial  NOT NULL,
    estado varchar(50)  NULL,
    estado_int int  NULL,
    CONSTRAINT PK_estados PRIMARY KEY (id)
);

-- Table: orden_compra
CREATE TABLE orden_compra (
    id serial  NOT NULL,
    codigo VARCHAR(40) NOT NULL UNIQUE,
    estado varchar(50)  NULL,
    fecha_descarga timestamp  NULL,
    fecha_envio timestamp  NULL,
    nombre varchar(4000)  NULL,
    descripcion text  NULL,
    moneda varchar(50)  NULL,
    anexos boolean  NULL,
    notas varchar(4000)  NULL,
    proveniente_de varchar(1000)  NULL,
    proveniente_licitacion varchar(50)  NULL,
    nombre_organismo_comprador varchar(4000)  NULL,
    nombre_unidad_comprador varchar(4000)  NULL,
    rut_comprador char(15)  NULL,
    region_comprador varchar(4000)  NULL,
    comuna_comprador varchar(4000)  NULL,
    dir_unidad_comprador varchar(4000)  NULL,
    nombre_contacto_comprador varchar(4000)  NULL,
    cargo_contacto_comprador varchar(4000)  NULL,
    fono_contacto_comprador varchar(50)  NULL,
    fax_contacto_comprador varchar(1000)  NULL,
    mail_contacto_comprador varchar(1000)  NULL,
    forma_pago varchar(1000)  NULL,
    razon_social_facturacion varchar(4000)  NULL,
    rut_facturacion char(15)  NULL,
    direccion_facturacion varchar(4000)  NULL,
    comuna_facturacion varchar(4000)  NULL,
    direccion_envio_factura varchar(4000)  NULL,
    metodo_despacho varchar(4000)  NULL,
    direccion_despacho varchar(4000)  NULL,
    fecha_entrega timestamp  NULL,
    nombre_proveedor varchar(4000)  NULL,
    razon_social_proveedor varchar(4000)  NULL,
    nombre_sucursal varchar(4000)  NULL,
    rut_proveedor char(15)  NULL,
    direccion_proveedor varchar(4000)  NULL,
    region_proveedor varchar(4000)  NULL,
    comuna_proveedor varchar(4000)  NULL,
    contacto_proveedor varchar(4000)  NULL,
    cargo_contacto_proveedor varchar(4000)  NULL,
    fono_contacto_proveedor varchar(4000)  NULL,
    fax_contacto_proveedor varchar(4000)  NULL,
    mail_contacto_proveedor varchar(4000)  NULL,
    total_neto numeric(30,3)  NULL,
    descuento numeric(30,3)  NULL,
    cargos numeric(30,3)  NULL,
    iva_porcentaje int  NULL,
    iva numeric(30,3)  NULL,
    impuesto numeric(30,3)  NULL,
    total numeric(30,3)  NULL,
    tiene_items boolean  NULL,
    cant_items numeric(30,3)  NULL,
    url varchar(1000)  NULL,
    gran_compra varchar(1000)  NULL,
    estado_int int  NULL,
    CONSTRAINT PK_Ordenes PRIMARY KEY (id, codigo)
);

-- Table: productos
CREATE TABLE productos (
    product_id serial  NOT NULL,
    orden_compra_codigo VARCHAR(40)  NOT NULL,
    onu bigint  NULL,
    proveniente_cm varchar(50)  NULL,
    producto_servicio text  NULL,
    cantidad numeric(30,3)  NULL,
    medida varchar(100)  NULL,
    esp_comprador text  NULL,
    esp_proveedor text  NULL,
    precio_unit numeric(30,3)  NULL,
    descuentos numeric(30,3)  NULL,
    cargos_2 numeric(30,3)  NULL,
    total_impuestos numeric(30,3)  NULL,
    total_unit numeric(30,3)  NULL,
    valor_total numeric(30,3)  NULL,
    codigo_producto_cm varchar(50)  NULL,
    CONSTRAINT PK_productos PRIMARY KEY (orden_compra_codigo,product_id)
);

-- foreign keys
-- Reference: FK_productos_orden_compra (table: productos)
ALTER TABLE productos ADD CONSTRAINT FK_productos_orden_compra
    FOREIGN KEY (orden_compra_codigo)
    REFERENCES orden_compra (codigo)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- End of file.

