USE [TestJan]
GO
/****** Object:  Table [dbo].[orden_compra]    Script Date: 02/29/2016 01:15:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[orden_compra](
	[id] [varchar](50) NOT NULL,
	[estado] [nvarchar](50) NULL,
	[fecha_descarga] [datetime2](0) NULL,
	[fecha_envio] [datetime2](0) NULL,
	[nombre] [nvarchar](4000) NULL,
	[descripcion] [nvarchar](max) NULL,
	[moneda] [nvarchar](50) NULL,
	[anexos] [bit] NULL,
	[notas] [nvarchar](4000) NULL,
	[proveniente_de] [nvarchar](1000) NULL,
	[proveniente_licitacion] [varchar](50) NULL,
	[nombre_organismo_comprador] [nvarchar](4000) NULL,
	[nombre_unidad_comprador] [nvarchar](4000) NULL,
	[rut_comprador] [char](15) NULL,
	[region_comprador] [nvarchar](4000) NULL,
	[comuna_comprador] [nvarchar](4000) NULL,
	[dir_unidad_comprador] [nvarchar](4000) NULL,
	[nombre_contacto_comprador] [nvarchar](4000) NULL,
	[cargo_contacto_comprador] [nvarchar](4000) NULL,
	[fono_contacto_comprador] [nvarchar](50) NULL,
	[fax_contacto_comprador] [nvarchar](1000) NULL,
	[mail_contacto_comprador] [nvarchar](1000) NULL,
	[forma_pago] [nvarchar](1000) NULL,
	[razon_social_facturacion] [nvarchar](4000) NULL,
	[rut_facturacion] [char](15) NULL,
	[direccion_facturacion] [nvarchar](4000) NULL,
	[comuna_facturacion] [nvarchar](4000) NULL,
	[direccion_envio_factura] [nvarchar](4000) NULL,
	[metodo_despacho] [nvarchar](4000) NULL,
	[direccion_despacho] [nvarchar](4000) NULL,
	[fecha_entrega] [datetime2](0) NULL,
	[nombre_proveedor] [nvarchar](4000) NULL,
	[razon_social_proveedor] [nvarchar](4000) NULL,
	[nombre_sucursal] [nvarchar](4000) NULL,
	[rut_proveedor] [char](15) NULL,
	[direccion_proveedor] [nvarchar](4000) NULL,
	[region_proveedor] [nvarchar](4000) NULL,
	[comuna_proveedor] [nvarchar](4000) NULL,
	[contacto_proveedor] [nvarchar](4000) NULL,
	[cargo_contacto_proveedor] [nvarchar](4000) NULL,
	[fono_contacto_proveedor] [nvarchar](4000) NULL,
	[fax_contacto_proveedor] [nvarchar](4000) NULL,
	[mail_contacto_proveedor] [nvarchar](4000) NULL,
	[total_neto] [numeric](30, 3) NULL,
	[descuento] [numeric](30, 3) NULL,
	[cargos] [numeric](30, 3) NULL,
	[iva_porcentaje] [int] NULL,
	[iva] [numeric](30, 3) NULL,
	[impuesto] [numeric](30, 3) NULL,
	[total] [numeric](30, 3) NULL,
	[tiene_items] [bit] NULL,
	[cant_items] [numeric](30, 3) NULL,
	[url] [nvarchar](1000) NULL,
	[gran_compra] [nvarchar](1000) NULL,
	[estado_int] [int] NULL,
 CONSTRAINT [PK_Ordenes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[estados]    Script Date: 02/29/2016 01:15:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[estados](
	[id] [varchar](50) NOT NULL,
	[estado] [nvarchar](50) NULL,
	[estado_int] [int] NULL,
 CONSTRAINT [PK_estados] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[productos]    Script Date: 02/29/2016 01:15:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[productos](
	[orden_compra_id] [varchar](50) NOT NULL,
	[product_id] [int] NOT NULL,
	[onu] [bigint] NULL,
	[proveniente_cm] [varchar](50) NULL,
	[producto_servicio] [nvarchar](max) NULL,
	[cantidad] [numeric](30, 3) NULL,
	[medida] [nvarchar](100) NULL,
	[esp_comprador] [nvarchar](max) NULL,
	[esp_proveedor] [nvarchar](max) NULL,
	[precio_unit] [numeric](30, 3) NULL,
	[descuentos] [numeric](30, 3) NULL,
	[cargos_2] [numeric](30, 3) NULL,
	[total_impuestos] [numeric](30, 3) NULL,
	[total_unit] [numeric](30, 3) NULL,
	[valor_total] [numeric](30, 3) NULL,
	[codigo_producto_cm] [varchar](50) NULL,
 CONSTRAINT [PK_productos] PRIMARY KEY CLUSTERED 
(
	[orden_compra_id] ASC,
	[product_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  ForeignKey [FK_productos_orden_compra]    Script Date: 02/29/2016 01:15:39 ******/
ALTER TABLE [dbo].[productos]  WITH NOCHECK ADD  CONSTRAINT [FK_productos_orden_compra] FOREIGN KEY([orden_compra_id])
REFERENCES [dbo].[orden_compra] ([id])
GO
ALTER TABLE [dbo].[productos] CHECK CONSTRAINT [FK_productos_orden_compra]
GO
