-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2017-04-29 18:50:34.007

-- tables
-- Table: orden_compra
CREATE TABLE orden_compra (
    id serial  NOT NULL,
    codigo VARCHAR(40) NOT NULL,
    estado varchar(50)  NULL,
    estado_int int  NULL,
    region_comprador varchar(4000)  NULL,
    comuna_comprador varchar(4000)  NULL,
    mail_contacto_comprador varchar(4000)  NULL,
    region_proveedor varchar(4000)  NULL,
    mail_contacto_proveedor varchar(4000)  NULL,
    CONSTRAINT PK_orden_compra_1 PRIMARY KEY (id, codigo)
);

-- End of file.

