USE [TestEstados]
GO
/****** Object:  Table [dbo].[orden_compra]    Script Date: 02/22/2017 05:04:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[orden_compra](
	[id] [varchar](50) NOT NULL,
	[nombre] [nvarchar](4000) NULL,
	[estado] [nvarchar](50) NULL,
	[estado_int] [int] NULL,
 CONSTRAINT [PK_orden_compra] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
