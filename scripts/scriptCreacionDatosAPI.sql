USE [DatosAPI]
GO
/****** Object:  Table [dbo].[orden_compra]    Script Date: 02/20/2017 05:09:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[orden_compra](
	[id] [varchar](50) NOT NULL,
	[estado] [nvarchar](50) NULL,
	[estado_int] [int] NULL,
	[region_comprador] [nvarchar](4000) NULL,
	[comuna_comprador] [nvarchar](4000) NULL,
	[mail_contacto_comprador] [nvarchar](4000) NULL,
	[region_proveedor] [nvarchar](4000) NULL,
	[mail_contacto_proveedor] [nvarchar](4000) NULL,
 CONSTRAINT [PK_orden_compra_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
