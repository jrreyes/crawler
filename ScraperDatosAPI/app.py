import crawler as ca
import ConfigParser
import logging
import sys
import traceback
import datetime as dt
import database as db

config = ConfigParser.ConfigParser()
config.read('config.ini')
inp = config.get('Obtainer', 'Input')

def find_missing(path_in, path_out):
    '''
    dado un archivo de input con ids de orden de compra
    se encuentran todos los codigos que no estan en la base de datos pero
    si en el archivo y se retornan.
    '''
    conn=db.Connection()
    out=open(path_out, 'w')
    try:
        with open(path_in) as inp:
            i=0
            for line in inp:
                arr = line.split()
                code = arr[0].strip()
                if not conn.in_database(code):
                    aux=line+'\n'
                    out.write(line)
                else:
                    i+=1
            print i
    finally:
        out.close()

if __name__ == '__main__':
    
   
    obt = ca.Obtainer()
    try:
        obt.obtain_data(inp)
    except:
        print traceback.format_exc()
        a = raw_input()


