import pyodbc
import logging
import ConfigParser

'''
Representa la conexion con la base de datos
Contiene multiples metodos para hacer distintas consultas
'''


class Connection:
    def __init__(self):
        config = ConfigParser.ConfigParser()
        config.read('config.ini')
        print 'Conectando a la base...'

        self._cnxn = pyodbc.connect('Trusted_Connection=yes', driver=config.get('Database', 'Driver'),
                                    server=config.get('Database', 'Server'), port=config.get('Database', 'Port'),
                                    database=config.get('Database', 'Database'), uid=config.get('Database', 'Uid'),
                                    pwd=config.get('Database', 'Pwd'))

        self._cursor = self._cnxn.cursor()
        
        self._insert_query = 'insert into orden_compra(codigo, estado, estado_int, region_comprador, comuna_comprador, mail_contacto_comprador, region_proveedor, mail_contacto_proveedor) values (?, ?, ?, ?, ?, ?, ?,?)'
        self._update_query = 'update orden_compra set codigo = ?, estado = ?, estado_int = ?, region_comprador = ?, comuna_comprador = ?, mail_contacto_comprador = ?, region_proveedor = ?, mail_contacto_proveedor = ? where codigo = ?'
        print 'Conexion lista'


    #Esto no salva race conditions, pueden haber problemas de concurrencia pero son riesgos muy bajos ya que suponemos que no habran deletes
    def insert(self,params):

        try:

            print self._insert_query
            print params
            self._cursor.execute(self._insert_query,params)
            self._cnxn.commit()
            logging.info('insert correcto de id %s' % params[0])

        except pyodbc.IntegrityError:
            logging.warning('%s ya esta en la base, se hara un update' % params[0])
            params.append(params[0])
            self._cursor.execute(self._update_query,params)
            self._cnxn.commit()
            logging.info('update correcto de id %s' % params[0])

    

    def update(self,params):
        param_upd=params[:]
        param_upd.append(params[0])
        num_affected=self._cursor.execute(self._update_query,param_upd).rowcount
        self._cnxn.commit()
        
        if num_affected==0:
            logging.warning('%s no esta en la base, se hara un insert' % params[0])
            self._cursor.execute(self._insert_query,params)
            self._cnxn.commit()
            logging.info('insert correcto de %s' % params[0])
            return
        
        logging.info('update correcto de %s' % params[0])


    def close(self):
        self._cnxn.close()


    def in_database(self,code):
        self._cursor.execute('select * from orden_compra where codigo = ?', code)
        row = self._cursor.fetchone()
        if row:
            return True
        return False