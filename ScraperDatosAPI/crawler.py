import requests
import database as db
import datetime as dt
import traceback
import logging
import time

class Obtainer:
    '''
    Clase que hace la obtencion de datos:
    '''
    def __init__(self):
        self.api_ticket = 'BF399BD6-1E1B-421F-9C2A-333076BFC7BC'
        self.connect()
        self.base = 'http://api.mercadopublico.cl/servicios/v1/publico/ordenesdecompra.json'

    def create_log(self,path):
        with open(path+'.log', 'w'):
            pass
        logging.basicConfig(filename=path+'.log', level=logging.INFO)
        logging.info('Started')

    def connect(self):
        self.conn = db.Connection()

    def obtain_data(self, inp):
        out = "files/output.txt"
        self.create_log(out)
        i = 0
        with open(out,"w") as outf:
                with open(inp) as f:
                    for line in f:
                        i+=1
                        code = line.strip()
                        url = '%s?codigo=%s&ticket=%s' % (self.base, code, self.api_ticket)
                        print i, url
                        tries = 0
                        cant = 0
                        while True:
                            try:
                                total = None
                                time.sleep(2)
                                tries += 1
                                req = requests.get(url)
                                total = req.json()
                                cant = total['Cantidad']
                            
                            except:
                                if tries < 5:
                                    logging.error("Error en %s, seguira intentando\n%s" % (code, traceback.format_exc()))
                                    continue
                                else:
                                    logging.error("Error en %s, NO se seguira intentando\n%s" % (code, traceback.format_exc()))
                                    outf.write("%s\n" % code)
                                    break

                            if cant == 0:
                                logging.error("Potencial error en %s, respuesta vacia\n" % (code))
                                outf.write("%s\n" % code)
                            else:
                                logging.info("Sin problemas en %s\n" % (code))
                            break

                        if total is not None and cant == 1:
                            cod = total['Listado'][0]
                            comprador = cod['Comprador']
                            params = [cod['Codigo'], cod['Estado'], cod['CodigoEstado'], comprador['RegionUnidad'], comprador['ComunaUnidad'], comprador['MailContacto'], cod['Proveedor']['Region'], cod['Proveedor']['MailContacto']]
                            self.conn.insert(params)

