import requests
import database as db
import datetime as dt
import traceback
import logging
import time

class Obtainer:
    '''
    Clase que hace la obtencion de datos:
    '''
    def __init__(self):
        self.api_ticket = 'BF399BD6-1E1B-421F-9C2A-333076BFC7BC'
        self.connect()
        self.base = 'http://api.mercadopublico.cl/servicios/v1/publico/ordenesdecompra.json'

    def create_log(self,path):
        with open(path+'.log', 'w'):
            pass
        logging.basicConfig(filename=path+'.log', level=logging.INFO)
        logging.info('Started')

    def connect(self):
        self.conn = db.Connection()

    def obtain_states_oc(self,out_oc,start_date,end_date):
        '''
        Obtencion de estados de ordenes de compra indicando fechas de inicio y termino
        '''
        self.create_log(out_oc)
        with open(out_oc,"w") as outf:
            total_days = (end_date - start_date).days + 1
            for day_number in range(total_days):
                
                current_date = (start_date + dt.timedelta(days = day_number)).date()
                date = self.change_date_format(current_date)
                print "Fecha: ", date
                url_date='http://api.mercadopublico.cl/servicios/v1/publico/ordenesdecompra.json?fecha='+date+'&ticket='+self.api_ticket
                mess='Fecha: '+date+' no pudo obtener info de la API para ordenes de compra'
                tries = 0
                cant = 0
                while True:
                    try:
                        total = None
                        time.sleep(2)
                        tries += 1
                        req = requests.get(url_date)
                        total = req.json()
                        cant = total['Cantidad']
                        print "Cantidad: ",cant
                    
                    except:
                        if tries < 5:
                            logging.error("Error en %s, seguira intentando\n%s" % (date, traceback.format_exc()))
                            continue
                        else:
                            logging.error("Error en %s, NO se seguira intentando\n%s" % (date, traceback.format_exc()))
                            outf.write("%s\n" % date)
                            break

                    if cant == 0:
                        logging.error("Potencial error en %s, respuesta vacia\n" % (date))
                        outf.write("%s\n" % date)
                    else:
                        logging.info("Sin problemas en %s\n" % (date))
                    break
                
                i = 0
                for cod in total['Listado']:
                    i += 1
                    params = [cod['Codigo'], cod['Nombre'] ,self.int_to_state(cod['CodigoEstado']), cod['CodigoEstado']]
                    self.conn.insert(params)

    def int_to_state(self, state_code):
        '''
        Transforma el codigo de estado a string
        '''
        if 4 == state_code:
            return 'Enviada a proveedor'
        if 6 == state_code:
            return 'Aceptada'
        if 9 == state_code:
            return 'Cancelada'
        if 15 == state_code:
            return 'Recepcion Conforme Incompleta'
        if 5 == state_code:
            return 'En proceso'
        if 12 == state_code:
            return 'Recepcion Conforme'
        if 13 == state_code:
            return 'Pendiente de Recepcionar'
        if 14 == state_code:
            return 'Recepcionada Parcialmente'

    def change_date_format(self, date):
        '''
        Transforma fecha a una aceptada por la api
        ''' 
        return self.transform_num(date.day)+self.transform_num(date.month)+str(date.year)

    def transform_num(self,num):
        '''
        permite transforman un numero a formato de 2 digitos o mas (Formato comun para consultar en la api)
        '''
        if num>=0 and num<10:
            return '0'+str(num)
        return str(num)


