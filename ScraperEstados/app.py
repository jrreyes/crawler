import crawler as ca
import ConfigParser
import logging
import sys
import traceback
import datetime as dt
import database as db

config = ConfigParser.ConfigParser()
config.read('config.ini')
outp = config.get('Obtainer', 'Output')
date1 = config.get('Obtainer', 'Date1')
date2 = config.get('Obtainer', 'Date2')


def string_to_date(date_text):
    '''
    Transforma un string a Date
    '''
    listDate=date_text.split('-')
    return dt.datetime(int(listDate[2]), int(listDate[1]), int(listDate[0]))

if __name__ == '__main__':
    obt = ca.Obtainer()
    try:
        obt.obtain_states_oc(outp,string_to_date(date1),string_to_date(date2))
    except:
        print traceback.format_exc()
        a = raw_input()